# UCS-Nifi Docker images
This repository contains the different docker images that can be used to deploy and start the [Nifi-based](https://bitbucket.org/cogmedsys/ucs-implementation) implementation of [UCS](http://www.hl7.org/implement/standards/product_brief.cfm?product_id=388).

## Building the images
Each individual image can be independently built but it is recommended to use [docker-compose](https://docs.docker.com/compose/).

Use `ucs-docker-build.sh` to build all the required images and `ucs-docker-start.sh` to start them. If you are planning to start this docker-compose bundle manually, please notice that the option `--x-networking` is required.

When ucs-nifi-docker image is created, it will mount the directory /ucs-nifi-docker/contact in /opt/nifi/conf/contact. This directory is where the configurtion of the contacts used for demo purposes can be configured.

## Getting the images from Dockerhub
The individual images contained in this repository are published in [Dockerhub](https://hub.docker.com/u/cognitivemedicine/dashboard/). The file `docker-compose.yml` can be modified to make use of these images instead of building them.

## Interacting with the images
When using **docker-compose**, the following applications/utilities can be accessed:

* **[ucs-nifi](https://bitbucket.org/cogmedsys/ucs-implementation)** implementation can be accessed using a web browser using the following address: http://localhost:6060/nifi
* **[ucs-nifi-test-workbench](https://bitbucket.org/cogmedsys/ucs-implementation/src/0199518601ffe6a111363e22775ce2ec63c05aab/ucs-nifi-samples/ucs-nifi-test-workbench/?at=master)** exposes 2 different endpoints:

    * The messages interface: http://localhost:7070/ucs-nifi-test-workbench 
    * The inbox interface: http://localhost:7070/ucs-nifi-test-workbench/inbox.html
  
The ports exposed by these 2 images are listed in the `docker-compose.yml` file.

When manually starting ucs-nifi-docker image, the volume host that will be mounted in /opt/nifi/conf/contact must be specified. For example:

`docker run -it --rm -p 6060:8080 -p 8888-8892:8888-8892 -v /some/place/contact:/opt/nifi/conf/contact cognitivemedicine/ucs:latest`
