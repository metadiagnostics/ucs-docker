#~/bin/bash

version=${1:-latest}

contact=`pwd`;
contact="$contact/contact"

echo "Version is $version"
echo "Contact directory is $contact"

docker run -it --rm -p 6060:8080 -p 8888-8892:8888-8892 -v $contact:/opt/nifi/conf/contact cognitivemedicine/ucs:$version 
